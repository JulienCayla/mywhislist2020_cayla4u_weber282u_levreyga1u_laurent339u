<?php

require_once 'vendor/autoload.php';

use mywishlist\controleur\ControllerFormulaire;
use mywishlist\controleur\ControllerList;
use mywishlist\controleur\ControllerItem;
use \mywishlist\vue\VueIndex;


use Illuminate\Database\Capsule\Manager as DB;
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();
$app->get('/',function(){
	echo VueIndex::render();
})->name('racine');

/*$app->get('/Item/:id',function($id){
	$c = new ControllerItem();
  $c->getItem();
});
*/
/*$app->get('/ItemParListe/:id_liste',function($id){
	$c = new ControllerItem();
  $c->getItemParListe();
});
*/
$app->get('/AjouterUneListeGet',function(){
	$c = new \mywishlist\controleur\ControllerFormulaire();
	$c->AjouterListgetForm();
})->name('AjouterUneListeGet');

$app->post('/AjouterUneListePost',function(){
	$c = new \mywishlist\controleur\ControllerFormulaire();
	$c->AjouterListPost();
})->name('AjouterUneListePost');


$app->get('/modif/:token',function($token){
	$c = new \mywishlist\controleur\ControllerItem();
	$c->AjouterItemGet($token);
})->name('ItemCreerr');

$app->post('/AjoutItem/:token',function($token){
	$c = new \mywishlist\controleur\ControllerItem();
	$c->AjouterItemPost($token);
})->name('ItemCreerPost');


$app->get('/visiteur/:partage',function($partage){
	$c = new \mywishlist\controleur\ControllerList();
	$c->AjouterItemPartage($partage);
})->name('AfficherListee');

$app->post('/afficherListPartagePost/:partage/:id', function($partage,$id){
	$c = new \mywishlist\controleur\ControllerList();
	$c->PostListPartage($partage,$id);
})->name('Abcd');

$app->get('/afficherMaList',function(){
	$c = new \mywishlist\controleur\ControllerList();
	$c->GetListAffichage();
})->name('AfficherMaListTok');

$app->post('/afficherMaList',function(){
	$c = new \mywishlist\controleur\ControllerList();
	$c->PostListAffichage();
});

$app->get('/LesListes', function(){
	$c = new \mywishlist\controleur\ControllerList();
	$c->AfficherToutesLesListes();
	});


$app->run();
