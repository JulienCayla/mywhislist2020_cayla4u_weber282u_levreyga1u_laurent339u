<?php

namespace mywishlist\models;
require_once ('vendor/autoload.php');

class Item extends
    \Illuminate\Database\Eloquent\Model
{
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function liste() {
       return $this->belongsTo( '\mywishlist\models\Liste','liste_id');
    }

/*    public function creerItem(){
      echo "<br/>";
      $item = new \mywishlist\models\Item();
      $item->nom = "Pistolet a eau";
      $item->save();
      echo $item->id." ".$item->liste_id."<br>";
      $item->liste_id = 1;
      $item->save();
      echo $item->id." ".$item->liste_id."<br>";
      echo'insertion reussie';
    }
*/

}
