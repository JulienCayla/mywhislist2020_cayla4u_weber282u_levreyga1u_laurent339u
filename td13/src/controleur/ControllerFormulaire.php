<?php

namespace mywishlist\controleur;
use mywishlist\vue\VueFormulaire;
use mywishlist\vue\VueParticipant;
use mywishlist\vue\VueAjouterListe;
use mywishlist\models\Liste;
use mywishlist\models\Item;

class ControllerFormulaire{

    public function AjouterListPost(){
    $app = \Slim\Slim::getInstance();
    $liste = new \mywishlist\models\Liste();
    $titre=$app->request->post('titre');
    $description=$app->request->post('description');
    $liste->titre = filter_var($titre, FILTER_SANITIZE_STRING);
    $liste->description = filter_var($description, FILTER_SANITIZE_STRING);
    $liste->expiration= filter_var($app->request->post('expiration'),FILTER_SANITIZE_NUMBER_INT);
    $liste->token=bin2hex(openssl_random_pseudo_bytes(8));
    $liste->partage=bin2hex(openssl_random_pseudo_bytes(8));
    $liste->user_id=random_int(1,999999);
    $liste->save();
    $liste= Liste::where('titre','=', $liste->titre)->first();
    $aff = new VueAjouterListe($liste, 'LIST_CREATE');
    echo $aff->render();
}
public function AjouterListGetForm(){
  $list=Liste::get();
  $v= new VueAjouterListe($list,'LIST_EDIT');
  $v->render();
}


}
