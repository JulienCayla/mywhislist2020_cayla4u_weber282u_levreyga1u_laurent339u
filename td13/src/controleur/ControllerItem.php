<?php

namespace mywishlist\controleur;
use mywishlist\vue\VueParticipant;
use mywishlist\models\Liste;
use mywishlist\models\Item;
use mywishlist\vue\VueAjouterItem;
class ControllerItem {


  public function getItemParListe(){
    $item=Item::get();
     $v = new VueParticipant($item,'ITEMLIST_VIEW');
     $v->render();
  }
  public function AjouterItemPost($token){
    $app = \Slim\Slim::getInstance();
    $nom =$app->request->post('nom');
    $desc =$app->request->post('description');
    $tarif =$app->request->post('tarif');
    $list=Liste::Get();
    $itemId = '';
   foreach($list as $list1){
    if ($list1->token == $token){
     $itemId = $list1->no;
  }
}

    $item = new Item();
    echo $itemId;

    $item->liste_id = $itemId;

    $item->nom = filter_var($nom, FILTER_SANITIZE_STRING);
    $item->descr = filter_var($desc, FILTER_SANITIZE_STRING);
    $item->tarif = filter_var($tarif, FILTER_SANITIZE_STRING);
    $item->save();
    $aff = new VueAjouterItem($list,'ITEM_CREATE',$token);
    echo $aff->render();
  }
  public function AjouterItemGet($tok){
    $list=Liste::get();
    $v= new VueAjouterItem($list,'ITEM_FORM',$tok);
    $v->render();
  }

  public function ReservGet($tok){
    $list=Liste::get();
    $v= new VueAjouterItem($list,'ITEM_PAR',$tok);
    $v->render();
  }


}
