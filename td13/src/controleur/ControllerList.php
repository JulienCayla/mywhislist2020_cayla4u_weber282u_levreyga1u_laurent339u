<?php

namespace mywishlist\controleur;
use mywishlist\vue\VueAjouterListe;
use mywishlist\vue\VueParticipant;
use mywishlist\models\Liste;
use mywishlist\models\Item;
use mywishlist\models\Reservation;
use mywishlist\vue\VueAfficherLesListes;

class ControllerList {
/*
  $listl = \mywishlist\models\Liste::all();
  $vue = new \mywishlist\vue\VueParticipant($listl->toArray());
  $vue->render();
*/

  public function getList() {
	 $list=Liste::get();
    $v = new VueParticipant($list,'LIST_VIEW');
    $v->render();
  }
  public function getUrlToken($tok){
    $list=Liste::Find($tok);
    $aff = new VueParticipant($list,'LIST_TOKEN');
    $aff->render();
  }

  public function AfficherToutesLesListes(){
    $list=Liste::Get();
    $tab;
    foreach ($list as $l){
      $tab []= $l;
    }
    $app = new VueAfficherLesListes($list, 'LES_LISTES');
    $app->addTab($tab);
    $app->render();
  }

  public function GetListAffichage(){
   $list=Liste::Get();
   $aff = new VueParticipant($list, 'LIST_AFF');
   echo $aff->render();
 }
  public function PostListAffichage(){
    $app = \Slim\Slim::getInstance();
    $tok =$app->request->post('token');
    $list=Liste::where('token','=',$tok)->first();
    $item = Item::where('liste_id','=',$list->no)->get();
    $par = $list->partage;
    $tab;
    $tab2;
    $reserv = \mywishlist\models\Reservation::get() ;
    foreach ($reserv as $r){
      $tab2[] = $r;
    }
    echo "L'id de la liste est : ".$list->no."<BR>"." Le titre de la liste est : ".$list->titre."<BR>"." La date d'expiration : ".$list->expiration."<BR>"."<BR>";;
    foreach ($item as $it) {
      $tab[] = $it;
    }
    $aff = new VueParticipant($list, 'LIST_AFFI');
    $aff->addPart($par);
    $aff->addTab($tab);
    $aff->addTab2($tab2);
    $aff->addTok($tok);
    $aff->render();
  }
  public function AjouterItemPartage($par){
    $list=Liste::where('partage','=', $par)->first();
    $liste=Liste::get();
    $item=Item::get();
    $aff = new VueParticipant($list,'LIST_PARTAGE');
    $t;$li;
    foreach ($item as $i) {
      $t [] = $i;
    }
    foreach ($liste as $l) {
      $li []= $l;
    }
    $aff->addPart($par);
    $aff->addTab2($li);
    $aff->addTab($t);
    $aff->render();
  }
  public function PostListPartage($par, $item_idd){
    $app = \Slim\Slim::getInstance();
    $list=Liste::where('partage','=', $par)->first();
      $nomU =$app->request->post('nomUtil');
      $m =$app->request->post('message');
      $res = new Reservation();
      $res->nomUtil = filter_var($nomU, FILTER_SANITIZE_STRING);
      $res->message = filter_var($m, FILTER_SANITIZE_STRING);
      $res->item_id =$item_idd;

   $res->save();
   $aff = new VueParticipant($list,'ITEM_REU');
   $aff->addPart($par);
  $aff->render();

  }
}
