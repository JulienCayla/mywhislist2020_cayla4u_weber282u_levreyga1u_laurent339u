<?php

namespace mywishlist\vue;

use mywishlist\controleur\ControllerList;

class VueAjouterItem {
  private $tab,$selecteur,$token;

   function __construct($t,$choix,$token){
  $this->tab=$t;
  $this->selecteur=$choix;
  $this->token = $token;
}

  public function render() {
    $app = \Slim\Slim::getInstance();
    $url = $app ->urlFor('racine');
    $url = $url."style.css";
    echo $url;
    switch ($this->selecteur) {
      case 'ITEM_FORM' : {
      $content = $this->afficherFormItem();
      break;
      }
      case 'ITEM_CREATE' : {
      $content = $this->Reussie();
    }
    }
$html = <<<END
<!DOCTYPE html>
<html>
<meta charset="utf-8"/>
     <link rel="stylesheet" href=$url>
<body>
<div class="content">
$content
</div>
</body></html>
END;
echo $html;
}
public function Reussie(){
return ('ajout réalisé avec succès');
}

public function afficherFormItem(){
  $app = \Slim\Slim::getInstance();
   $url =  $app->urlFor("racine")."AjoutItem/".$this->token;
   echo $url;
//  $url = $app ->urlFor('ItemCreerPost');
  return <<<FIN
    <div class="ajtl">
    <p> Veuillez remplir le formulaire suivant pour ajouter un item </p>
      <form class="formL" method="post" action="$url">
          <p> Nom de l'item <input type="text" name="nom" required/> </p>
          <p> Description de l'item <input type="text" name="description" required/></p>
          <p> Prix de l'item : <input type="text" name="tarif" required/> </p>
    <input type="submit" value="Listee" class="button">
      </form>
      </div>
FIN;

}
}
