<?php

namespace mywishlist\vue;

use mywishlist\controleur\ControllerList;

 class VueParticipant {

  private $tab,$selecteur;
  protected $html;
  protected $partage;
  private $tabaf,$tabaf2;
  private $token;

  function __construct($t,$choix){
  $this->tab=$t;
  $this->selecteur=$choix;
  $this->partage=0;
  $this->tabaf =0;
  $this->tabaf2 =0;
  $this->token=0;
}

public function addTab($t){
  $this->tabaf = $t;
}
public function addTab2($t){
  $this->tabaf2 = $t;
}
public function addPart($p){
  $this->partage=$p;
}
public function addTok($t){
  $this->token =$t;
}



public function render() {
  $app = \Slim\Slim::getInstance();
  $url = $app ->urlFor('racine');
  $url = $url."style.css";
  switch ($this->selecteur) {

    case 'ITEMLIST_VIEW' : {
    $content = $this->ItemDansListeSouhait(2);
    break;
    }
    case 'LIST_TOKENVIEW' : {
        $content = $this->afficherListFromToken();
        break;
    }
    case 'LIST_PARTAGE' : {
      $content = $this->afficherListPartage();
      break;
    }
    case 'ITEM_CREATE' : {
      $content = $this->afficherFormItem();
      break;
    }
    case 'ITEM_REU' : {
      $content = $this->Reussie();
      break;
    }
    case 'LIST_AFF' : {
      $content = $this->GetAff();
      break;
    }
    case 'LIST_AFFI': {
      $content = $this->Affichermesitems();
      break;
    }
    case 'LES_LISTES': {
      $content= $this->AfficherToutesListes();
      break;
    }
}
$html = <<<END
<!DOCTYPE html>
<html>
 <meta charset="utf-8"/>
      <link rel="stylesheet" href=$url>
<body>
<div class="content">
$content
</div>
</body></html>
END;
echo $html;

}
public function AfficherToutesListes(){
  $content ="";
  foreach($this->tabaf as $t){
    $content = $content." Numéro de la liste : ".($t->no)." Titre de la liste : ".($t->titre)." Description de La liste : ".$t->description." date d'expiration : ".$t->expiration. "<BR>";
  }
  return $content;
}

public function Affichermesitems(){
  $app = \Slim\Slim::getInstance();
  $i=0;
  foreach($this->tabaf as $t){
    $i=$i+1;
    if($t->img != null){
    echo "item numéro ".$i." , "."Nom de l'item : ".$t->nom." description : ".$t->descr." id :  ".$t->id." ".'<img src="/www/cayla4u/td13/img/'.$t->img.'"width =100 height=100"/><br>';;
  } else {
      echo "item numéro ".$i." , "."Nom de l'item : ".$t->nom." description : ".$t->descr." id :  ".$t->id." , pas d'image".'<br>';;
  }
  foreach($this->tabaf2 as $t2){
    if($t->id == $t2->item_id){
    echo "l'item est réservé par :    ".$t2->nomUtil." le message : ".$t2->message. "<BR>"."<BR>";;
    }
  }}
  $this->html.= '<ul class="items">';
  $this->html.= '</ul>';
  $url = $app ->urlFor('racine');
  $urlP = $url."modif/".$this->token;
  if (!$this->partage == null){
  $url2 = $url."visiteur/".$this->partage;
  $this->html.= <<<FIN
          <h3>
          votre lien de modification pour ajouter un item est : <a href="$urlP">$urlP</a>
          </h3>
          <h3>
          votre lien de partage est : <a href="$url2">$url2</a>
          </h3>
FIN;
} else {
  $this->html.= <<<FIN
          <h3>
          votre lien de modification pour ajouter un item est : <a href="$urlP">$urlP</a>
          </h3>
FIN;

}
echo $this->html;
}
public function getAff(){
  $app = \Slim\Slim::getInstance();
  $url = $app->urlFor('AfficherMaListTok');
  return <<<FIN
      <form class="formL" method="post" action="$url">
          <p> Token/code de modification de la liste <input type="text" name="token" required/></p>
    <input type="submit" value="Chercher" class="button">
      </form>
FIN;
}


public function afficherListPartage(){
  $app = \Slim\Slim::getInstance();
  $liste_id = $this->tab->no;
  $url = $app->urlFor('racine');
  $content = "<h4>".'vous est sur le lien de partage'."</h4>"."<BR>";;
  $content = $content.'le titre est : '.$this->tab->titre." "."<BR>";;
  $content = $content.'la description est : '.$this->tab->description."<BR>";;
  foreach($this->tabaf2 as $list){
    if($list->no == $liste_id){
    $content=$content."le numéro de la liste : ".$list->no."<BR>"."<BR>";

    foreach($this->tabaf as $itemm){

      if ($itemm->liste_id == $liste_id){

        $content1 =" item numéro : ".$itemm->id.", nom de l'item : ".$itemm->nom.", description de l'item : ".$itemm->descr." ,prix de l'item : ".$itemm->tarif." euros"."<BR>";
        echo $content1;
        if($this->estReserv($itemm->id) == 1){

        $url ='/td13/afficherListPartagePost/'.$this->partage."/".$itemm->id;
        echo <<<FIN
        <form class="ajt" method="post" action="$url">
                <p> Nom d'utilisateur : <input type="text" name="nomUtil" required /></p>
                <p> Message :  <input type="text" name="message" required /></p>
                <p> <input type="submit" value="ok"></p>
             </form>
FIN;
} else {
  echo "l'item est déja reservé"."<BR>"."<BR> ";
}
      }
    }
  }


  }

}
public function estReserv($item_id){
  $return = 1;
    $reserv = \mywishlist\models\Reservation::get() ;
    foreach ($reserv as $res) {
      if($res->item_id==$item_id){
        $return = 2;
      }
    }
    return $return;

}
public function Reussie(){
    $app = \Slim\Slim::getInstance();
    $url = $app ->urlFor('racine');
    $url2 = $url."visiteur/".$this->tab->partage;
    $this->html = "<h2 class=\"titreL\">{$this->tab->titre}</h2>";
    //var_dump($items);
    $this->html.= '<ul class="items">';
    $this->html.= '</ul>';
    $this->html.= <<<FIN
            <h4>
                pour retourner sur le lien de partage : <a href="$url2">$url2</a>
                </h4>
FIN;
  echo $this->html ;



}

public function afficherListFromToken(){
  $app = \Slim\Slim::getInstance();

  $content = 'vous est sur le lien de modification'."<BR>";;
  $content = $content.'le titre est : '.$this->tab->titre." "."<BR>";;
  $content = $content.'la description est :'.$this->tab->description;



  return $content ;
}





}
