<?php

namespace mywishlist\vue;
use mywishlist\controleur\ControllerList;
class VueAfficherLesListes{

  private $tab,$selecteur;
  protected $html;
  protected $partage;
  private $tabaf,$tabaf2;
  private $token;

  function __construct($t,$choix){
  $this->tab=$t;
  $this->selecteur=$choix;
  $this->partage=0;
  $this->tabaf =0;
  $this->tabaf2 =0;
  $this->token=0;
}

public function render() {
  $app = \Slim\Slim::getInstance();
  $url = $app ->urlFor('racine');
  $url = $url."style.css";
  switch ($this->selecteur) {
    case 'LES_LISTES': {
      $content= $this->AfficherToutesListes();
      break;
    }
}
$h=header::header();
$html = <<<END
<!DOCTYPE html>
<html>
<div class ="Listee">
<h2 class ="titreliste"> Les Listes Existantes </h2>
</div>
$h
 <meta charset="utf-8"/>
      <link rel="stylesheet" href=$url>
<body>

<div class="content">
$content
</div>
</body></html>
END;
echo $html;

}



public function addTab($t){
  $this->tabaf = $t;
}


public function AfficherToutesListes(){
  $p = "<p>";
  $pp = "</p>";
  $s = "<strong>";
  $ss= "</strong>";
  $content ="";
  foreach($this->tabaf as $t){
    $content = $content.$p.$s."Numéro de la liste : ".$ss.($t->no).$pp.$p.$s." Titre de la liste : ".$ss.($t->titre).$pp.$p.$s." Description de La liste : ".$ss.$t->description.$pp.$p.$s." date d'expiration : ".$ss.$t->expiration.$pp."<BR>";
  }
  return $content;
}
}
