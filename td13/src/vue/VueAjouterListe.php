<?php

namespace mywishlist\vue;

use mywishlist\controleur\ControllerList;

class VueAjouterListe {
  private $tab,$selecteur;
  protected $html;

   function __construct($t,$choix){
  $this->tab=$t;
  $this->selecteur=$choix;
  }

  public function render() {
    $app = \Slim\Slim::getInstance();
    $url = $app ->urlFor('racine');
    $url = $url."style.css";
    switch ($this->selecteur) {
      case 'LIST_EDIT' : {
      $content = $this->AjouterUneListeGetFormulaire();
      break;
      }
      case 'LIST_CREATE' : {
      $content = $this->Reussie();
    }
    }
$html = <<<END
<!DOCTYPE html>
<html>
<meta charset="utf-8"/>
     <link rel="stylesheet" href=$url>
<body>
<div class="content">
$content
</div>
</body></html>
END;
echo $html;
}
public function Reussie(){
  $app = \Slim\Slim::getInstance();
  $this->html= "<h3>La liste à bien été créée</h3>"."Titre : ".$this->tab->titre."<BR>"."Le code qui vous permettra de retrouver votre liste si vous la perdez (elle doit contenir au moins un item) : ".$this->tab->token."<BR>";
  $this->html.= '<ul class="yo">';
  $this->html.= '</ul>';
  $url = $app ->urlFor('racine');
  $url2 = $url."visiteur/".$this->tab->partage;
  $urlP =$url."modif/".$this->tab->token;
  $this->html.= <<<FIN
          <h4>
              votre lien de modification est : <a href="$urlP">$urlP</a>
          </h4>
          <h4>
              votre lien de partage est : <a href="$url2">$url2</a>
              </h4>

              Ces deux liens sont très précieux, ne les perdez pas ! En particulier celui de modification. <br>
              Ce dernier permets de retrouver le lien de partage, d'afficher tous ses item ainsi que les réservation. <br>
              Le lien de partage lui permets aux visiteurs de participer à votre liste.
FIN;
  return $this->html;
}

public function AjouterUneListeGetFormulaire(){

  $app = \Slim\Slim::getInstance();
  $url = $app->urlFor('AjouterUneListePost');
  return <<<FIN
  <div class="backform">
      <div class="ajtl">
      <p> Veuillez remplir le formulaire suivant pour créer une liste </p>
      <form class="formL" method="post" action="$url">
          <p> Nom de la liste <input type="text" name="titre" required/> </p>
          <p> Description de la liste <input type="text" name="description" required/></p>
          <p> Date : <input type="date" name="expiration" required/> </p>
    <input type="submit" value="Créer" class="button">
      </form>
      </div>
      </div>
FIN;




}//, ['token'=>$liste->token]
}
